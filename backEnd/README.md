# ARP-BackEnd

## Bulletproof project architecture

- The back end of the application is built in Node.js using Express.js framework.
- The architecture respects the principle of separation of concerns by handling all the business logic in the services layer, away from the routes layer.
- The connection between the database and the services is made through the models layer, containing two main entities: User and Drug.

## Configurations and secrets

- Configured CORS for the application.
- Used passport with local and jwt strategies middleware for secure user authentication.
- Used bcrypt to hash user passwords and secret question answers.

## Routing

- Created the main routes for the application and tested the CRUD operations on the database using Postman.