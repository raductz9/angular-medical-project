import mongoose from "mongoose";
import { databaseURL } from "../config/config";

export default class AppService {
    private MONGO_STRING: string = databaseURL;
    private static _instance: AppService;

    private constructor() { }

    public static get Instance() {
        if (this._instance) {
            return this._instance;
        } else {
            this._instance = new AppService();
            return this._instance;
        }
    }

    public connect() {
        return new Promise((resolve, reject) => {
            mongoose.connect(this.MONGO_STRING, {
                useNewUrlParser: true,
                useFindAndModify: false,
                useCreateIndex: true
            });
            let db = mongoose.connection;
            db.once("open", () => {
                console.log("Connected to " + this.MONGO_STRING);
                resolve();
            });
        });
    }

    public disconnect() {
        mongoose.disconnect();
    }
}
