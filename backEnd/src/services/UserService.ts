import User from "../models/UserModel";

export default class UserService {
  private static _instance: UserService;

  private constructor() {}

  public static get Instance() {
    if (this._instance) {
      return this._instance;
    } else {
      this._instance = new UserService();
      return this._instance;
    }
  }

  public async getUserByUsername(username: string) {
    return await User.findOne({ username: username }).exec();
  }

  public async getUserByEmail(email: string) {
    return await User.findOne({ email: email }).exec();
  }

  public async addNewUser(username: string, password: string, email: string) {
    let newUser = new User({
      username: username,
      password: password,
      email: email
    });

    return await newUser.save();
  }
}
