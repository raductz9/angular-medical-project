import passport from "passport";
import passportLocal from "passport-local";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import User from "../models/UserModel";
import { secret } from "../config/config";
import validator from "validator";

const LocalStrategy = passportLocal.Strategy;

passport.use(
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password"
    },
    async (email, password, done) => {
      try {
        if (validator.isEmail(email)) {
          let user = await User.findOne({ email: email }).exec();
          if (user) {
            return done(null, user.toObject());
          } else {
            return done(null, false);
          }
        } else {
          return done(null, false);
        }
      } catch {
        return done(null);
      }
    }
  )
);

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: secret
    },
    async (jwtPayload, done) => {
      try {
        let user = await User.findOne({ _id: jwtPayload._id }).exec();
        if (user) {
          return done(null, user);
        } else {
          return done(null, false);
        }
      } catch (err) {
        return done(err, false);
      }
    }
  )
);
