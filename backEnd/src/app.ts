import express from "express";
import Server from "./server/Server";
import AppService from "./services/AppService";
import { port } from "./config/config";

const app = express();

AppService.Instance.connect().then(() => {
    new Server(app, port);
});

export default app;
