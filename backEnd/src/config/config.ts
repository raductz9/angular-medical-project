import dotenv from "dotenv";

dotenv.config();

export const port: number = parseInt("8080");
export const secret: string = "notSoSecret";
export const databaseURL: string = "mongodb://localhost:27017/Medical-Project";
export const clientURL: string = "http://localhost:4200";
