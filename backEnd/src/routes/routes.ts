import express from "express";
import UserService from "../services/UserService";
import * as jwt from "jsonwebtoken";
import { secret } from "../config/config";
import validator from "validator";

import * as HttpStatus from "http-status-codes";

export default (app: express.Application, passport?: any) => {
  app.post(
    "/login",
    passport.authenticate("local", {
      session: false
    }),
    (req: express.Request, res: express.Response) => {
      if (req.user) {
        let token = jwt.sign(req.user, secret);
        res.status(HttpStatus.OK).json({
          message: "Login sucessful!",
          token: token,
          status: HttpStatus.getStatusText(HttpStatus.OK)
        });
      } else {
        res.status(HttpStatus.UNAUTHORIZED).json({
          message: "Login failed!",
          status: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED)
        });
      }
    }
  );

  app.post("/register", async (req: express.Request, res: express.Response) => {
    try {
      if (
        validator.isEmail(req.body.email) &&
        validator.isLength(req.body.password, 8, 16)
      ) {
        if (
          (await UserService.Instance.getUserByUsername(req.body.username)) ||
          (await UserService.Instance.getUserByEmail(req.body.email))
        ) {
          res.status(HttpStatus.NOT_ACCEPTABLE).json({
            message: "Error! Username or password already exists!",
            status: HttpStatus.getStatusText(HttpStatus.NOT_ACCEPTABLE)
          });
        } else {
          await UserService.Instance.addNewUser(
            req.body.username,
            req.body.password,
            req.body.email
          );
          res.status(HttpStatus.OK).json({
            message: "Account sucessfully created!",
            status: HttpStatus.getStatusText(HttpStatus.OK)
          });
        }
      } else {
        res.status(HttpStatus.NOT_ACCEPTABLE).json({
          message:
            "Your email is not an email or password length is not accepted!",
          status: HttpStatus.getStatusText(HttpStatus.NOT_ACCEPTABLE)
        });
      }
    } catch (e) {
      console.log(e);
      res.status(HttpStatus.PRECONDITION_FAILED).json({
        message: "Error! Cannot create account.",
        status: HttpStatus.getStatusText(HttpStatus.PRECONDITION_FAILED)
      });
    }
  });
};
