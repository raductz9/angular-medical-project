import { Component, OnInit } from "@angular/core";
import { BaseComponent } from "../base/base.component";
import { UserService } from "src/app/services/user/user.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent extends BaseComponent implements OnInit {
  constructor(userService: UserService, router: Router) {
    super(userService, router);
  }

  ngOnInit() {}

  async getStatus(): Promise<boolean> {
    return await this.getUserService.register(
      this.username,
      this.password,
      this.email.toLowerCase()
    );
  }

  async onClickRegister(): Promise<void> {
    let status = await this.getStatus();
    this.onClick(status);
  }
}
