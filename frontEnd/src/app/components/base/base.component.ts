import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/user/user.service";
import validator from "validator";

export abstract class BaseComponent implements OnInit {
  username: string;
  email: string;
  password: string;

  isEmailError: boolean = false;
  isPasswordError: boolean = false;

  successShown: boolean = false;
  errorShown: boolean = false;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {}

  public get getUserService(): UserService {
    return this.userService;
  }

  public get getRouter(): Router {
    return this.router;
  }

  abstract async getStatus(): Promise<boolean>;

  onClick(isSuccess: boolean, newPassword?: string): void {
    this.validateEmail(this.email);
    this.validatePassword(this.password || newPassword);
    if (isSuccess) {
      this.successShown = true;
      setTimeout(() => {
        this.router.navigate(["/patients-data"]);
      }, 1000);
    } else {
      this.errorShown = true;
      setTimeout(() => {
        this.errorShown = false;
      }, 1500);
    }
  }

  validateEmail(email: string): void {
    if (validator.isEmail(email)) {
      this.isEmailError = false;
    } else {
      this.isEmailError = true;
    }
  }

  validatePassword(password: string): void {
    const minLength: number = 8;
    const maxLength: number = 16;

    if (validator.isLength(password, minLength, maxLength)) {
      this.isPasswordError = false;
    } else {
      this.isPasswordError = true;
    }
  }
}
