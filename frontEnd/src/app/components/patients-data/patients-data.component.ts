import { Component, OnInit, ViewChild } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";

export interface Patients {
  firstName: string;
  secondName: string;
  id: number;
  sex: string;
  adress: string;
  city: string;
  postal: number;
  birthday: string;
  diagnosis: string;
}

const ELEMENT_DATA: Patients[] = [
  {
    id: 45,
    firstName: "Popescu",
    secondName: "Andrei",
    sex: "M",
    adress: "Str. Telecabinei, 1",
    city: "Sinaia",
    postal: 106100,
    birthday: "10/03/1998",
    diagnosis: "Pneumonia",
  },
  {
    id: 32,
    firstName: "Stelian",
    secondName: "Maria",
    sex: "F",
    adress: "Str. Mihai Viteazu, 3",
    city: "Sinaia",
    postal: 106100,
    birthday: "17/05/1997",
    diagnosis: "Heart Failure",
  },
  {
    id: 2,
    firstName: "Vladescu",
    secondName: "Catalin",
    sex: "M",
    adress: "Str. Valea Alba, 36",
    city: "Brasov",
    postal: 105110,
    birthday: "28/03/1962",
    diagnosis: "Diabetes",
  },
  {
    id: 1,
    firstName: "Suteu",
    secondName: "Elena",
    sex: "F",
    adress: "Str. Salciilor, 15",
    city: "Sinaia",
    postal: 106100,
    birthday: "05/05/2003",
    diagnosis: "Abdominal Pain",
  },
  {
    id: 4,
    firstName: "Cojoc",
    secondName: "Stefania",
    sex: "F",
    adress: "Str. Trandafirilor, 2",
    city: "Brasov",
    postal: 105110,
    birthday: "29/07/1987",
    diagnosis: "Smallpox",
  },
  {
    id: 63,
    firstName: "Gagescu",
    secondName: "Ion",
    sex: "M",
    adress: "Str. Lunga, 62",
    city: "Brasov",
    postal: 105110,
    birthday: "06/08/1976",
    diagnosis: "Abdominal Pain",
  },
  {
    id: 8,
    firstName: "Titulescu",
    secondName: "Alexandru",
    sex: "M",
    adress: "Str. Operei, 25",
    city: "Rasnov",
    postal: 105110,
    birthday: "23/12/1977",
    diagnosis: "Diabetes",
  },
  {
    id: 9,
    firstName: "Cojoianu",
    secondName: "Bianca",
    sex: "F",
    adress: "Str. Liliacului, 14",
    city: "Bucuresti",
    postal: 205206,
    birthday: "03/03/1983",
    diagnosis: "Pulmonary Disease",
  },
  {
    id: 105,
    firstName: "Stelian",
    secondName: "Alexandru",
    sex: "M",
    adress: "Str. Walter, 21",
    city: "Brasov",
    postal: 105110,
    birthday: "17/09/1970",
    diagnosis: "Pulmonary Disease",
  },
  {
    id: 22,
    firstName: "Ezaru",
    secondName: "Mihai",
    sex: "M",
    adress: "Str. Azugii, 31",
    city: "Azuga",
    postal: 126100,
    birthday: "01/05/1999",
    diagnosis: "Diarrhea",
  },
  {
    id: 103,
    firstName: "Visean",
    secondName: "Alexandru",
    sex: "M",
    adress: "Str. Pietii, 8",
    city: "Brasov",
    postal: 105110,
    birthday: "30/09/1999",
    diagnosis: "Chest Pain",
  },
  {
    id: 108,
    firstName: "Teodorescu",
    secondName: "Diana",
    sex: "F",
    adress: "Str. Teilor, 66",
    city: "Azuga",
    postal: 126100,
    birthday: "04/12/1989",
    diagnosis: "High Blood Pressure",
  },
  {
    id: 69,
    firstName: "Stancu",
    secondName: "Adrian",
    sex: "M",
    adress: "Str. Apollo, 21",
    city: "Brasov",
    postal: 105110,
    birthday: "06/09/1998",
    diagnosis: "Headache",
  },
  {
    id: 127,
    firstName: "Olteanu",
    secondName: "Mihaela",
    sex: "F",
    adress: "Str. Garii, 5",
    city: "Busteni",
    postal: 101000,
    birthday: "03/07/1980",
    diagnosis: "Hypertension",
  },
  {
    id: 90,
    firstName: "Gatej",
    secondName: "Andrei",
    sex: "M",
    adress: "Str. Malului, 2",
    city: "Sinaia",
    postal: 106100,
    birthday: "01/01/2011",
    diagnosis: "Diarrhea",
  },
  {
    id: 24,
    firstName: "Lung",
    secondName: "Alexandra",
    sex: "F",
    adress: "Str. Lunii, 11",
    city: "Mehedinti",
    postal: 225200,
    birthday: "03/03/1989",
    diagnosis: "Covid-19",
  },
];

@Component({
  selector: "app-patients-data",
  templateUrl: "./patients-data.component.html",
  styleUrls: ["./patients-data.component.css"],
})
export class PatientsDataComponent implements OnInit {
  displayedColumns: string[] = [
    "id",
    "firstName",
    "secondName",
    "sex",
    "adress",
    "city",
    "postal",
    "birthday",
    "diagnosis",
  ];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }
}
