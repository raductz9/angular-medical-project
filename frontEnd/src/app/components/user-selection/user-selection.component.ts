import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-user-selection",
  templateUrl: "./user-selection.component.html",
  styleUrls: ["./user-selection.component.css"]
})
export class UserSelectionComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  openLogin() {
    let modal = document.getElementById("loginModal") as HTMLElement;
    modal.style.display = "block";
  }

  closeLogin() {
    let modal = document.getElementById("loginModal") as HTMLElement;
    modal.style.display = "none";
  }

  openRegister() {
    let modal = document.getElementById("registerModal") as HTMLElement;
    modal.style.display = "block";
  }

  closeRegister() {
    let modal = document.getElementById("registerModal") as HTMLElement;
    modal.style.display = "none";
  }
}
