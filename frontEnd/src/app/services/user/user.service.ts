import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { API_URL } from "src/config/config";
import { TOKEN_KEY } from "src/config/config";

@Injectable({
  providedIn: "root"
})
export class UserService {
  static readonly API_URL = API_URL;
  static readonly TOKEN_KEY = TOKEN_KEY;

  constructor(private httpClient: HttpClient) {}

  set token(token: string) {
    localStorage.setItem(UserService.TOKEN_KEY, token);
  }

  get token() {
    return localStorage.getItem(UserService.TOKEN_KEY);
  }

  set email(email: string) {
    localStorage.setItem("email", email);
  }

  set username(username: string) {
    localStorage.setItem("username", username);
  }

  set password(password: string) {
    localStorage.setItem("password", password);
  }

  set newPassword(newPassword: string) {
    localStorage.setItem("newPassword", newPassword);
  }

  login(email: string, password: string) {
    let body = new HttpParams().set("email", email).set("password", password);
    return this.httpClient
      .post(`${UserService.API_URL}/login`, body)
      .toPromise()
      .then((response: any) => {
        this.token = response.token;
        this.email = email;
        return true;
      })
      .catch(err => {
        return false;
      });
  }

  register(username: string, password: string, email: string) {
    let body = new HttpParams()
      .set("username", username)
      .set("password", password)
      .set("email", email);
    return this.httpClient
      .post(`${UserService.API_URL}/register`, body)
      .toPromise()
      .then((response: any) => {
        this.username = username;
        this.password = password;
        this.email = email;
        return true;
      })
      .catch(err => {
        return false;
      });
  }
}
