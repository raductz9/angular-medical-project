// import { TestBed, inject, fakeAsync } from "@angular/core/testing";
// import {
//   HttpClientTestingModule,
//   HttpTestingController
// } from "@angular/common/http/testing";
// import { UserService } from "./user.service";

// describe("UserService", () => {
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       imports: [HttpClientTestingModule],
//       providers: [UserService]
//     });
//   });

//   //test if register pass correctly
//   it("should perform register correctly", fakeAsync(
//     inject([UserService, HttpTestingController], (authService: UserService) => {
//       //Mock the response
//       const promise = new Promise<boolean>((resolve, reject) => {
//         resolve(true);
//       });

//       spyOn(authService, "register").and.returnValue(promise);
//       //Set up mock data
//       const userMail = "userTest@example.com";
//       const userPassword = "testPass";
//       const username = "testUsername";
//       const securityQuestion = "What is a promise?";
//       const securityQuestionAnswer = "testResponse";

//       authService.register(username, userPassword, userMail).then(resp => {
//         expect(resp.valueOf()).toBe(true);
//       });
//     })
//   ));

//   //test if register fail
//   it("should perform register badly", fakeAsync(
//     inject([UserService, HttpTestingController], (authService: UserService) => {
//       //mock the response
//       const promise = new Promise<boolean>((resolve, reject) => {
//         resolve(false);
//       });

//       spyOn(authService, "register").and.returnValue(promise);
//       //Set up the mock data
//       const userMail = "userTest@example.com";
//       const userPassword = "testPass";
//       const username = "testUsername";
//       const securityQuestion = "What is a promise?";
//       const securityQuestionAnswer = "testResponse";

//       authService
//         .register(
//           username,
//           userPassword,
//           userMail,
//           securityQuestion,
//           securityQuestionAnswer
//         )
//         .then(resp => {
//           expect(resp.valueOf()).toBe(false);
//         });
//     })
//   ));

//   //test if recover pass correctly
//   it("should recover password correctly", fakeAsync(
//     inject([UserService, HttpTestingController], (authService: UserService) => {
//       //mock the response
//       const promise = new Promise<boolean>((resolve, reject) => {
//         resolve(true);
//       });

//       spyOn(authService, "recover").and.returnValue(promise);
//       //Set up the mock data
//       const userMail = "userTest@example.com";
//       const newPassword = "testPass";
//       const securityQuestion = "What is a promise?";
//       const securityQuestionAnswer = "testResponse";

//       authService
//         .recover(
//           userMail,
//           securityQuestion,
//           securityQuestionAnswer,
//           newPassword
//         )
//         .then(resp => {
//           expect(resp.valueOf()).toBe(true);
//         });
//     })
//   ));

//   //test if recover fail
//   it("should recover password badly", fakeAsync(
//     inject([UserService, HttpTestingController], (authService: UserService) => {
//       //mock the response
//       const promise = new Promise<boolean>((resolve, reject) => {
//         resolve(false);
//       });

//       spyOn(authService, "recover").and.returnValue(promise);
//       //Set up the mock data
//       const userMail = "userTest@example.com";
//       const newPassword = "testPass";
//       const securityQuestion = "What is a promise?";
//       const securityQuestionAnswer = "testResponse";

//       authService
//         .recover(
//           userMail,
//           securityQuestion,
//           securityQuestionAnswer,
//           newPassword
//         )
//         .then(resp => {
//           expect(resp.valueOf()).toBe(false);
//         });
//     })
//   ));

//   //test if login is success
//   it("should login success", fakeAsync(
//     inject([UserService, HttpTestingController], async function(
//       authService: UserService
//     ) {
//       //mock the response
//       const promise = new Promise<boolean>((resolve, reject) => {
//         resolve(true);
//       });

//       spyOn(authService, "login").and.returnValue(promise);
//       //Set up the mock data
//       const userMail = "john@example.com";
//       const password = "12345678";

//       let response = await authService.login(userMail, password);
//       expect(response).toBe(true);
//     })
//   ));

//   //test if login fails
//   it("should login fail", fakeAsync(
//     inject([UserService, HttpTestingController], async function(
//       authService: UserService
//     ) {
//       //mock the response
//       const promise = new Promise<boolean>((resolve, reject) => {
//         resolve(false);
//       });

//       spyOn(authService, "login").and.returnValue(promise);
//       //set up the mock data
//       const userMail = "john@example.com";
//       const password = "12345678";

//       let response = await authService.login(userMail, password);
//       expect(response).toBe(false);
//     })
//   ));
// });
