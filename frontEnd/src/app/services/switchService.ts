export enum UserComponents {
    H = "H",
    unitbvLearningC1 = "uniC1",
    unitbvLearningC2 = "uniC2",
    unitbvLearningC3 = "uniC3",
    unitbvLearningC4 = "uniC4",
    unitbvLearningC5 = "uniC5",
    unitbvLearningC6 = "uniC6",
    unitbvLearningC7 = "uniC7",
    unitbvLearningC8 = "uniC8",
    unitbvLearningC9 = "uniC9",
    unitbvLearningC10 = "uniC10",
    unitbvLearningC11 = "uniC11",
    unitbvLearningC12 = "uniC12",
    unitbvLearningC13 = "uniC13",
    unitbvLearningC14 = "uniC14"
}
