import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {}

export class MainPageComponent implements OnInit {
  successShown: boolean = false;
  errorShown: boolean = false;

  constructor(private router: Router) {}

  ngOnInit() {}
}
