export default class User {
  username: string;
  email: string;
  password: string;

  constructor(username: string, email: string) {
    this.username = username;
    this.email = email;
  }
}
