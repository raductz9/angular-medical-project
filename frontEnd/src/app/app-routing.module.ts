import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UserSelectionComponent } from "./components/user-selection/user-selection.component";
import { PatientsDataComponent } from "./components/patients-data/patients-data.component";

const routes: Routes = [
  { path: "", component: UserSelectionComponent },
  { path: "patients-data", component: PatientsDataComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
