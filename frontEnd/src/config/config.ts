import * as dotenv from "dotenv";

dotenv.config();

export const BASE_URL = process.env.BASE_URL || "http://localhost:8080";
export const API_URL = process.env.API_URL || "http://localhost:8080";
export const TOKEN_KEY = process.env.TOKEN_KEY || "MY_TOKEN";